# Instructions for the human
print("Think of a number between 1 and 50")
guesses = []


from random import randint
low = 1
high = 50

result = ""

for guess_number in range(5):

    # Generate a random number between 1 and 50.
    guess = randint(low, high)

    # print("guess number: ", guess_number)
    # print("guess: ", guess)

    # Check if response is equal, higher, or lower
    response = input(f"Is your number equal, higher, or lower than {guess}.")

    if response == "equal":
        result = "equal"
        print("I guessed it!")
        break
    elif response == "higher":
        result = "too high"
        # scenario for higher
        print("The guess was too low.")
        low = guess + 1
        guesses.append([guess_number, result])
    elif response == "lower":
        result = "too low"
        print("The guess was too high.")
        high = guess - 1
        guesses.append([guess_number, result])

for item in guesses:
    print(guesses[0], item[0])

if result == "equal":
    print("We won the game together!")
elif result == "too high":
    print("Le sigh, my guess was too high.")
elif result == "too low":
    print("Le show, my guess was too low.")
